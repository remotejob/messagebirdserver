package domains

import "time"

type Hooklog struct {
	Timestamp time.Time `json:"timestamp"`
	Items     []struct {
		Type    string `json:"type"`
		Event   string `json:"event"`
		Payload struct {
			ID          string `json:"id"`
			Status      string `json:"status"`
			Source      string `json:"source"`
			Destination string `json:"destination"`
			Webhook     struct {
				URL   string `json:"url"`
				Token string `json:"token"`
			} `json:"webhook"`
			CreatedAt time.Time `json:"createdAt"`
			UpdatedAt time.Time `json:"updatedAt"`
			EndedAt   time.Time `json:"endedAt"`
		} `json:"payload"`
	} `json:"items"`
}