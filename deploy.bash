
# wsserver
#  systemctl enable  messagebirdserver.service
# scp -r media/*.mp3   root@165.227.101.53:messagebirdserver/media/soundfimp3/
scp -r soundfimp3/callback/*.mp3  root@165.227.101.53:messagebirdserver/media/soundfimp3/callback/
scp messagebirdserver.config.toml root@165.227.101.53:messagebirdserver/

go build -o bin/msgserver gitlab.com/remotejob/messagebirdserver/cmd/msgserver

systemctl stop messagebirdserver.service

scp bin/msgserver root@165.227.101.53:messagebirdserver/

systemctl start messagebirdserver.service

journalctl -f -u  messagebirdserver.service

#23.11.2020 Integration 
