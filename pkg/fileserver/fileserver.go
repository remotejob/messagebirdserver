package fileserver

import (
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strings"

	"github.com/go-chi/chi"
)



// type Config struct {
// 	*config.Config
// }

// func New(configuration *config.Config) *Config {
// 	return &Config{configuration}
// }

// func (config *Config) Routes() *chi.Mux {




// 	// router := chi.NewRouter()

// 	// router.Post("/", config.Webhookhandler)
// 	// // router.Get("/{uuid}/{phone}/{ask}", config.GetAnswer)
// 	// return router
// }

// func (config *Config) Webhookhandler(w http.ResponseWriter, r *http.Request) {


// 	body, err := ioutil.ReadAll(r.Body)
// 	if err != nil {
// 		log.Panicln(err)
// 	}
// 	bodyString := string(body)
// 	log.Println(bodyString)
// 	log.Println("----------------------------")

// }




func FileServer(r chi.Router, public string, static string) {

	if strings.ContainsAny(public, "{}*") {
		panic("FileServer does not permit URL parameters.")
	}

	root, _ := filepath.Abs(static)
	if _, err := os.Stat(root); os.IsNotExist(err) {
		panic("Static Documents Directory Not Found")
	}

	fs := http.StripPrefix(public, http.FileServer(http.Dir(root)))

	if public != "/" && public[len(public)-1] != '/' {
		r.Get(public, http.RedirectHandler(public+"/", 301).ServeHTTP)
		public += "/"
	}

	r.Get(public+"*", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		file := strings.Replace(r.RequestURI, public, "/", 1)
		if _, err := os.Stat(root + file); os.IsNotExist(err) {
			http.ServeFile(w, r, path.Join(root, "index.html"))
			return
		}
		fs.ServeHTTP(w, r)
	}))
}