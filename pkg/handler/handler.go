package handler

import (
	"io/ioutil"
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/remotejob/messagebirdserver/internal/config"
)

type Config struct {
	*config.Config
}

func New(configuration *config.Config) *Config {
	return &Config{configuration}
}

func (config *Config) Routes() *chi.Mux {
	router := chi.NewRouter()

	router.Post("/webhook", config.Webhookhandler)
	// router.Get("/{uuid}/{phone}/{ask}", config.GetAnswer)
	return router
}

func (config *Config) Webhookhandler(w http.ResponseWriter, r *http.Request) {


	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Panicln(err)
	}
	bodyString := string(body)
	if _, err := config.Calllog.WriteString(bodyString+"\n"); err != nil {
		log.Println(err)
	}

}