module gitlab.com/remotejob/messagebirdserver

go 1.14

require (
	github.com/fsnotify/fsnotify v1.4.9
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-chi/cors v1.1.1
	github.com/go-chi/render v1.0.1
	github.com/spf13/viper v1.7.0
)
