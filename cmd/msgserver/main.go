package main

import (
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"github.com/go-chi/render"
	"gitlab.com/remotejob/messagebirdserver/internal/config"
	"gitlab.com/remotejob/messagebirdserver/pkg/fileserver"
	"gitlab.com/remotejob/messagebirdserver/pkg/handler"
)

func Routes(configuration *config.Config) *chi.Mux {
	router := chi.NewRouter()

	cors := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
	})

	router.Use(
		render.SetContentType(render.ContentTypeJSON), // Set content-Type headers as application/json
		cors.Handler,
		middleware.Logger,          // Log API request calls
		// middleware.DefaultCompress, // Compress results, mostly gzipping assets and json
		middleware.RedirectSlashes, // Redirect slashes to no slash URL versions
		middleware.Recoverer,       // Recover from panics without crashing server
	)

	router.Route("/v0", func(r chi.Router) {
		r.Mount("/", handler.New(configuration).Routes())
	})


	fileserver.FileServer(router, "/media/soundfimp3/callback/", "media/soundfimp3/callback/")
	// router.Route("/media", func(r chi.Router) {
	// 	r.Mount("/", handler.New(configuration).Routes())
	// })

	// router.Route("/v2", func(r chi.Router) {
	// 	r.Mount("/api/chat", handlerv2.New(configuration).Routes())
	// })
	// router.Route("/v0", func(r chi.Router) {
	// 	r.Mount("/mldata", mldatahandlerv0.New(configuration).Routes())
	// })
	return router
}



func main() {

	configuration, err := config.New()
	if err != nil {
		log.Panicln("Configuration error", err)
	}
	router := Routes(configuration)

	walkFunc := func(method string, route string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) error {
		log.Printf("%s %s\n", method, route) // Walk and print out all routes
		return nil
	}
	if err := chi.Walk(router, walkFunc); err != nil {
		log.Panicf("Logging err: %s\n", err.Error()) // panic if there is an error
	}

	log.Println("Serving application at PORT :" + configuration.Constants.PORT)
	log.Fatal(http.ListenAndServe(":"+configuration.Constants.PORT, router))
	// http.HandleFunc("/smsstat", HelloServer)
	// http.ListenAndServe(":9000", nil)
}

// func HelloServer(w http.ResponseWriter, r *http.Request) {

// 	log.Println(r.Body)

// 	body, err := ioutil.ReadAll(r.Body)
// 	if err != nil {
// 		log.Panicln(err)
// 	}
// 	bodyString := string(body)
// 	log.Println(bodyString)
// 	log.Println("----------------------------")

// 	// fmt.Fprintf(w, "Hello, %s!", r.URL.Path[1:])
// }
