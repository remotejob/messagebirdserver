package main

import (
	"bufio"
	"encoding/json"
	"log"
	"os"

	"gitlab.com/remotejob/messagebirdserver/internal/domains"
)

func main() {
	file, err := os.Open("logs/calls.jsonl")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	hooklog := domains.Hooklog{}

	callsmap := make(map[string]string)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {

		err := json.Unmarshal([]byte(scanner.Text()), &hooklog)

		if err != nil {
			panic(err)
		}

		if len(hooklog.Items) == 1 {

			payload := hooklog.Items[0].Payload
			// log.Println(hooklog.Items[0].Payload)
			callsmap[payload.Destination] = payload.Status

		} else {

			log.Println("Items > 1 !!!!??")
		}

	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	for k, v := range callsmap {

		// if v == "hangup" {
			log.Println(k, v)
		// }
	}

	log.Println(len(callsmap))

}
